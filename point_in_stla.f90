module point_in_stl
  implicit none
  integer ( kind = 4 ) solid_num
  integer ( kind = 4 ) node_num
  integer ( kind = 4 ) face_num

  integer :: inside =  -1
  integer :: outside =  1
  real ( kind = 8 ) vmin(3),vmax(3)
  real ( kind = 8 ), parameter :: eps=1.0d-1
  
  logical :: verbose = .false.
  logical :: master = .true.

  ! Primary data arrays to allocate for STL geometry
  integer ( kind = 4 ), allocatable, dimension(:,:) :: face_node
  real ( kind = 8 ),    allocatable, dimension(:,:) :: face_normal
  real ( kind = 8 ),    allocatable, dimension(:,:) :: node_xyz

  interface piSTL_setup
     module procedure point_in_stl_setup
  end interface piSTL_setup
  
  interface piSTL_free
     module procedure point_in_stl_free
  end interface piSTL_free

contains  

  !=======================================================================
  !  STL reader & setup routines
  !=======================================================================
  subroutine point_in_stl_setup(input_file_name , verbosity, master)
    implicit none
    character ( len = * ), intent(in) ::  input_file_name
    logical, optional, intent(in) :: verbosity
    logical, optional, intent(in) :: master
    integer ( kind = 4 ) ierror
    integer ( kind = 4 ) text_num
    integer ( kind = 4 ) face, node 
    
    ! Set verbosity if given, else default (false)
    if (present(verbosity)) then
       verbose = verbosity
    end if

    call check_format(input_file_name)
    call stla_size(input_file_name,solid_num,node_num,face_num,text_num)
    
    allocate ( face_node(3,face_num) )
    allocate ( face_normal(3,face_num) )
    allocate ( node_xyz(3,node_num) )
    
    call stla_read (input_file_name,node_num,face_num,node_xyz,&
         face_node, face_normal, ierror)
    
    if ( ierror /= 0 .and. master) then
       write ( *, '(a,i8)' ) '  STLA_READ returned IERROR = ', ierror
       stop
    end if

    if (verbose) then
       call stla_size_print (input_file_name, solid_num, node_num, face_num, &
            text_num )
       call stla_face_node_print ( face_num, face_node )
       call stla_face_normal_print ( face_num, face_normal )
       call stla_node_xyz_print ( node_num, node_xyz )
    end if
    
    ! Computing minima and maxima values of X,Y, and Z for bounding cube
    ! This will be the first filter used in PIP_solver.  
    vmin = minval(node_xyz,dim=2) 
    vmax = maxval(node_xyz,dim=2)
    
  end subroutine point_in_stl_setup
  !=====================================================================



  !=====================================================================
  ! PIP solver in a single Point
  !=====================================================================
  subroutine point_in_stl_solver(qpt,io) 
    
    !Solves the Point in PolyHedron problem.
 
    implicit none
    integer ( kind = 4 ) face
    integer ( kind = 4 ) node
    integer ( kind = 4 ) io,counter,icount
    
    real ( kind = 8 ),intent (in) :: qpt(3)
    real ( kind = 8 ) ztest(3,face_num)
    real ( kind = 8 ) A(2),B(2),C(2)
    real ( kind = 8 ) AC(2),AB(2),AP(2) 
    real ( kind = 8 ) dotABAB,dotACAC,dotABAC,dotABAP,dotACAP
    real ( kind = 8 ) u,v,den, zpp, zA, zB, zC
    real ( kind = 8) zppc(200)
    real ( kind = 8) epsL
    real ( kind = 8 ) A3(3),B3(3),C3(3)
    
    epsL = eps
    
    !filtering the qpt in bounding cube XY:
    !qpts in the surfaces are considered 'outside' when  --> gt and lt
    !qpts in the surfaces are considered 'inside' when  --> ge and le
    if(qpt(1).gt.vmin(1).and.qpt(1).lt.vmax(1).and.&
         qpt(2).gt.vmin(2).and.qpt(2).lt.vmax(2).and.&
         qpt(3).gt.vmin(3).and.qpt(3).lt.vmax(3)) then
       !'The query point is inside the bounding cube XYZ'
       counter=0
       
       do face=1,face_num
          ztest(1,face)=node_xyz(3,face_node(1,face))
          ztest(2,face)=node_xyz(3,face_node(2,face))
          ztest(3,face)=node_xyz(3,face_node(3,face))
          
          !Filtering surfaces with any zcoordinate upper and equal than zqpt 
          if(ztest(1,face).ge.qpt(3).or.&
               ztest(2,face).ge.qpt(3).or.&
               ztest(3,face).ge.qpt(3))then     
             
             !surfaces purely vertical i.e. face_normal(3,:)=0, aren't 
             !proyected in 2D   
             if(face_normal(3,face).ne.0.0)then
                
                !Point in Triangle - Barycentric Method.
                !2D projection - its considered (x,y)coordinates.  
                A=node_xyz(1:2,face_node(1,face))   
                B=node_xyz(1:2,face_node(2,face))
                C=node_xyz(1:2,face_node(3,face))
                !Subtraction of positions arrays
                AC=C-A
                AB=B-A
                AP=qpt(1:2)-A
                !Dot product.
                dotABAB=dot_product(AB,AB)
                dotACAC=dot_product(AC,AC)  
                dotABAC=dot_product(AB,AC)
                dotABAP=dot_product(AB,AP)
                dotACAP=dot_product(AC,AP)
                !solves relatives parameters
                den=(dotACAC*dotABAB - dotABAC*dotABAC)
                u=(dotABAB*dotACAP - dotABAC*dotABAP)/den
                v=(dotACAC*dotABAP - dotABAC*dotACAP)/den
                
                !Computing the Zcoordinate piercing point 
                zA = node_xyz(3,face_node(1,face))
                zB = node_xyz(3,face_node(2,face))
                zC = node_xyz(3,face_node(3,face))
                
                zpp = (1.d0-u)*(1.d0-v) * zA + &
                     (u)     *(1.d0-v) * zC + &
                     (1.d0-u)*(v     ) * zB + &
                     (u)     *(v     ) * (zB + zC - zA )
                
                
                ! Tolerance on u,v coordinates
                A3=node_xyz(:,face_node(1,face))   
                B3=node_xyz(:,face_node(2,face))
                C3=node_xyz(:,face_node(3,face))
                epsL = MIN( SQRT(SUM((A3(1:2)-B3(1:2))**2)), &
                     SQRT(SUM((C3(1:2)-B3(1:2))**2)), &
                     SQRT(SUM((A3(1:2)-C3(1:2))**2)) )        
                epsL = epsL * .00001d0
                !epsL = 1.0D-7
                
                
                !check if the Query point is inside the triangle
                if(u.ge. -epsL .and. v.ge. -epsL .and.&
                     (u+v).le.1.0d0 + epsL .and.zpp.ge.qpt(3))then
                   
                   ! Tolerance on unique z pp
                   epsL = MIN( SQRT(SUM((A3-B3)**2)), &
                        SQRT(SUM((C3-B3)**2)), &
                        SQRT(SUM((A3-C3)**2)) )        
                   epsL = epsL * .001d0
                   
                   !First Piercing 
                   if(counter.eq.0)then 
                      
                      !'surface pierced:',counter+1, 'times'   
                      counter=counter+1
                      zppc(counter)=zpp
                      
                   ! Counter+1 Piercing
                   elseif(counter.gt.0)then

                      ! Loop over all unique ppoints
                      do icount=1,counter
                         
                         if(zpp.ge.(zppc(icount)-epsL).and.&
                              zpp.le.(zppc(icount)+epsL))then
                            if (  qpt(1) == 23.0 ) then
                               print*,'Edge repeated', real(zpp,kind=4)
                            end if
                            exit ! Stop at first non-unique
                         endif
                         
                         if(icount.eq.counter)then
                            !unique piercing point.
                            counter=counter+1
                            zppc(counter)=zpp
                         end if
                         
                      end do
                      
                   endif
                          
                endif

             endif
             
          endif
          
       enddo
       

    else
       !'The query point is outside the bounding cube XYZ and surface as well'
       io = outside
       return 
    endif
    
    if(mod(counter,2).gt.0)then
       !'Query point is inside the surface'
       io = inside
    else
       !'Query point is outside the surface'  
       io = outside
       !write(*,*) 'io:',io
    endif
            
    return
    
  end subroutine point_in_stl_solver
  !===============================================================
  
  
  !===============================================================
  ! Check the ASCII format of the input file
  !===============================================================
  subroutine check_format(input_file_name)
    implicit none
    character ( len = * ) input_file_name
    logical stla_check
    
    if ( stla_check ( input_file_name ) ) then

       if (verbose .and. master) then
          
          write ( *, '(a)' ) '  The file "' // trim ( input_file_name ) // &
               '" seems to be a legal ASCII STL file.'
          
       end if
    else

       if (verbose) then

          write ( *, '(a)' ) '  The file "' // trim ( input_file_name ) // &
               '" does NOT seem to be a legal ASCII STL file.'

       end if
    end if
    
    return
    
  end subroutine check_format
  !===============================================================

  
  !===============================================================
  ! Free up the library and deallocate all the data
  !===============================================================
  subroutine point_in_stl_free
    
    implicit none
    
    deallocate(node_xyz)
    deallocate(face_node)
    deallocate(face_normal)
    
  end subroutine point_in_stl_free
  !===============================================================
  


  !===============================================================
  ! Wrapper function that return [-1]:inside or 
  !    [1]:outside the surface
  !===============================================================
  function piSTL_query(qpt)
    real( kind = 8 ), dimension (3), intent(in) :: qpt
    integer :: piSTL_query
    
    call point_in_stl_solver(qpt,piSTL_query)
    
  end function piSTL_query
  !===============================================================


  
end module point_in_stl
