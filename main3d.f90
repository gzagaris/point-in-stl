program miranda
use point_in_stl
implicit none
real ( kind = 8 ) qpt(3)
integer ( kind = 4 ) io

integer :: nx,ny,nz,i,j,k
double precision, dimension(:,:,:), allocatable :: xgrid,ygrid,zgrid,pip
double precision :: xmin,xmax,ymin,ymax,zmin,zmax
character(len=120) :: stl_file, out_file
character(len=120) :: arg
integer :: flipYZ

stl_file = 'naca0012.stl'
out_file = 'naca0012.tec'
nx = 200
ny = 200
nz = 200
xmin = -1
xmax = 2
ymin = -.2
ymax = .2
zmin = -.2
zmax = .2

! Get CLI
IF (iargc() .ne. 11 ) THEN
   print*, 'Invalid arguments:'
   print*, './pistl stlfile x1 xn y1 yn z1 zn nx ny nz outfile'
   STOP
END IF

! STL file
CALL getarg(1,arg)
stl_file = arg
print*, 'STLFILE:  ', TRIM(stl_file)

! Extents
CALL getarg(2,arg)
read(arg,*) xmin
CALL getarg(3,arg)
read(arg,*) xmax
print*, 'X-range:  ',xmin,xmax

CALL getarg(4,arg)
read(arg,*) ymin
CALL getarg(5,arg)
read(arg,*) ymax
print*, 'Y-range:  ',ymin,ymax

CALL getarg(6,arg)
read(arg,*) zmin
CALL getarg(7,arg)
read(arg,*) zmax
print*, 'Z-range:  ',zmin,zmax

! Grid spacing
CALL getarg(8,arg)
read(arg,*) nx
CALL getarg(9,arg)
read(arg,*) ny
CALL getarg(10,arg)
read(arg,*) nz
print*, 'Grid size:  ',nx,ny,nz


! Outfile
CALL getarg(11,arg)
read(arg,*) out_file
print*, 'OUTFILE:  ', TRIM(out_file)

allocate(xgrid(nx,ny,nz))
allocate(ygrid(nx,ny,nz))
allocate(zgrid(nx,ny,nz))
allocate(pip  (nx,ny,nz))


do i=1,nx
   xgrid(i,:,:) = xmin + (xmax-xmin) / dble(nx) * dble(i-1)
end do

do i=1,ny
   ygrid(:,i,:) = ymin + (ymax-ymin) / dble(ny) * dble(i-1)
end do

do i=1,nz
   zgrid(:,:,i) = zmin + (zmax-zmin) / dble(nz) * dble(i-1)
end do


call point_in_stl_setup(TRIM(stl_file))


do i=1,nx
   do j=1,ny
      do k=1,nz
         qpt = (/ xgrid(i,j,k), ygrid(i,j,k), zgrid(i,j,k) /)       
         call point_in_stl_solver(qpt,io)
         pip(i,j,k) = io
      end do
   end do
   print*, i
end do

call point_in_stl_free


!Plotter
!============================================================================

open(200,file=TRIM(out_file))

write(200,*) 'variables= "x" , "y" , "z" , "io"'
write(200,*) 'ZONE, I= ',nx,' J=',ny,' K=', nz,' F=POINT'


do k=1,nz
   do j=1,ny
      do i=1,nx
         write(200,*) xgrid(i,j,k),ygrid(i,j,k), zgrid(i,j,k), pip(i,j,k)
      end do
   end do
end do

close(200)


end program
