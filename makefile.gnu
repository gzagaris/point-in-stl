#f90 = ifort
f90 = gfortran
flags = #-O3
exec = pistl
exec3d = pistl3d

source = stlasubs.o point_in_stla.o main.o
source3d = stlasubs.o point_in_stla.o main3d.o

$(exec) : $(source) $(source3d)
	$(f90) $(source)  $(flags) -o $(exec)
	$(f90) $(source3d)  $(flags) -o $(exec3d)

%.o: %.f90
	$(f90) $(flags) -c $<

clean:
	rm -f *.o *.mod $(exec) $(exec3d)
