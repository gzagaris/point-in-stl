f90 = ifort
#f90 = gfortran
flags = -O3
exec = pistl
lib = piSTL

top := $(shell pwd)
Lflags = -L$(top)/lib -lpiSTL


source = stlasubs.o point_in_stla.o
testSrc = main.o


$(lib) :  $(source)
	mkdir -p lib
	mkdir -p include
	ar rv lib/lib$(lib).a *.o
	cp *.mod include/.

$(exec) : $(testSrc)
	$(f90) $(flags) $(testSrc) $(Lflags)  -o $(exec)	

%.o: %.f90
	$(f90) $(flags) -c $<

clean:
	rm -rf lib *.o *.mod $(exec)
